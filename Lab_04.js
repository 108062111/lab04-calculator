console.log("Hello!");

var isEval = false;

function reset(){
    document.getElementById("screen").value = "0";
}

function updateText(c){
    var ogText = document.getElementById("screen").value;
    if(isEval){
        isEval = false;
        document.getElementById("screen").value = c;
    }
    else if(ogText == "error" || ogText == "Infinity"){
        document.getElementById("screen").value = c;
    }
    else if(ogText == "0" && c != "."){
        document.getElementById("screen").value = c;
    }
    else{
        document.getElementById("screen").value = ogText + c;
    }
}

function delText(){
    var ogText = document.getElementById("screen").value;
    if(ogText.length == 1){
        reset();
    }
    else{
        document.getElementById("screen").value = ogText.slice(0, -1);
    }
}

function myEval(){
    isEval = true;
    var ogText = document.getElementById("screen").value;
    try{
        result = eval(ogText);
        document.getElementById("screen").value = result;
    }
    catch(e){
        document.getElementById("screen").value = "error";
    }
}

function myKeydown(){
    const k = event.key;
    if("0" <= k  && k <= "9"){
        updateText(k);
    }
    else if(k == "/" || k == "*" || k == "-" || k == "+" || k == "."){
        updateText(k);
    }
    else if(k == "Enter"){
        myEval();
    }
    else if(k == "Backspace"){
        delText();
    }
}